from datetime import datetime, timedelta, date
import os
import sys
from MagneticSensor import MagneticSensor
import time

def get_date():
    hour = int(datetime.now().strftime('%H'))
    if(hour<6):
        yesterday = date.today() - timedelta(days = 1)
        date_string = yesterday.strftime("%m %d, %Y").split()
    else:
        date_string = date.today().strftime("%m %d, %Y").split()
    return(date_string)


def data_logger_directory():
    date_string = get_date()
    day_saving_path = (
        f"D:/Magnetic_Field_log/{date_string[2]}/{date_string[0]}/{date_string[1][:-1]}/"
    )
    if not os.path.isdir(day_saving_path):
        os.makedirs(day_saving_path)
    return day_saving_path


def log_magnetic_field(sensor_name: str, port: str, occurence: str):
    sensor = MagneticSensor(port, sensor_name)
    sensor.connect()
    sensor.reset_buffer()
    sensor.measure_field()
    while True:
        current_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        sensor.reset_buffer()
        value = sensor.measure_field()
        file = f"{data_logger_directory()}{sensor_name}.txt"
        if not value == None:
            with open(file, "a") as temp_file:
                temp_file.write(f"{str(current_time)}\t{value}\n")
        time.sleep(int(occurence))


if __name__ == "__main__":
    sensor_name = sys.argv[1]
    port = sys.argv[2]
    occurence = sys.argv[3]
    log_magnetic_field(sensor_name,port,occurence)# -*- coding: utf-8 -*-
    