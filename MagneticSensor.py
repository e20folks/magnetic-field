from errors import SensorNotConnected
import colorama

colorama.init()
from colorama import Fore
import serial
import time
import numpy as np
import re #regular expression to avoid errors on writting field


class MagneticSensor:
    def __init__(self, port: str, name: str):
        self.port = port
        self.name = name
        self.connected = False

    def connect(self):
        try:
            self.serialCom = serial.Serial(
                self.port, baudrate=2000000, timeout=1
            )  # initialise la comunication avec le port USB, timeout 1 second
            self.connected = True
            print(
                f"{Fore.CYAN}Magnetic Sensor {self.name} properly connected.\n ***********\n {Fore.RESET}"
            )

        except:
            self.connected = False
            raise SensorNotConnected(
                f"{Fore.RED}Error establishing communication with magnetic field source.\n ***********\n Port:{self.port} \n Name:{self.name} \n {Fore.RESET}"
            )

    def disconnect(self):
        if not self.connected:
            raise SensorNotConnected(
                f"{Fore.RED}Sensor {self.name} is already disconnected.{Fore.RESET}"
            )
        self.connected = False
        self.serialCom.close()
        self.serialCom = None
        print(
                f"{Fore.YELLOW}Magnetic Sensor {self.name} properly disconnected.\n ***********\n {Fore.RESET}"
            )

    def measure_field(self, threshold = 3): #The Serial Protocol introduces a lot of error in measurements that we have to clean. threshold is in microtesla
        if not self.connected:
            raise SensorNotConnected(
                f"{Fore.RED}Error. Sensor {self.name} is disconnected, measure was not taken.{Fore.RESET}"
            )
        try:
            format_float =  r"[-+]?(?:\d+\.\d{2})" #format of a float with two decimals
            format_measure = (3*f"{format_float} ")[:-1] #format of a measure with 3 floats
            full_last_measurements = self.serialCom.read(300).decode("utf-8") #we have something like  b' 8.50 41.10\r\n-28.95 8.50 41.10\r\n-28.95 8.50 41.10\r' We get something like 10 values of measures
            list_of_messages = full_last_measurements.split("\r\n") #The idea is to get a message with no error, so between two \r\n strings
            filtered_list = [measure for measure in list_of_messages if re.compile(format_measure).match(measure)] #filter the list to get only the messages with the correct format
            data = np.zeros((len(filtered_list), 3))
            for i, measure in enumerate(filtered_list):
                data[i] = np.array([float(x) for x in measure.split(" ")])  #convert the string to a list of floats
            index_to_discard = []
            for k in range(3):
                median = np.median(data[:,k]) #median is more sensitive to outliers than the mean
                index_to_discard += np.where(np.abs(data[:,k] - median) > threshold)[0].tolist() #get the indexes of the values that are too far from the median, it is something like a "majority" vote
            index_to_discard = list(set(index_to_discard)) #remove duplicates           
            final_list = [filtered_list[i] for i in range(len(filtered_list)) if i not in index_to_discard]
            if len(final_list) == 0:
                self.serialCom.cancel_read()
                print(f"{Fore.RED} Message not received.{Fore.RESET}")
                return None
            message = final_list[0] #take the first value
            return message #return the string to set in the log
        except:
            self.serialCom.cancel_read()
            print(f"{Fore.RED} Message not received.{Fore.RESET}")
            return None


    def reset_buffer(self):
        if self.connected:
            self.serialCom.reset_input_buffer()
            self.serialCom.reset_output_buffer()
