#%%
import pandas as pd
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import datetime
import math
import sys

sys.path.append("../scripts/logging_scripts")
from handle_log_files import get_files_between_dates

magnetic_sensors = ["Near_atoms_B_field", "Room_B_field"]
# magnetic_sensors = ["Near_atoms_B_field"]
# magnetic_sensors = ["Room_B_field"]
threshold = 3  # to clean spikes coming from the sensors, in µT
rolling_window = 3  # window size to determine if a data point is a spike or not


def get_magnetic_fields(date_1: str, date_2: str):
    rootdir = "//CDF-8N1TQD3/Data/Magnetic_Field_log"
    i = 0
    result = {}
    for sensor in magnetic_sensors:
        files_to_plot = get_files_between_dates(rootdir, sensor, date_1, date_2)
        for file in files_to_plot:  # put everything on a pandas dataframe
            with open(Path(file), "r") as log_data:
                for line in log_data:
                    line = line.replace("\n", " ").replace("\t", " ")
                    columns = line.split(" ")[:-1]
                    if len(columns) == 5:
                        data = {}
                        try:
                            data[f"Bx"] = float(columns[2])
                            data[f"By"] = float(columns[3])
                            data[f"Bz"] = float(columns[4])
                            data["B"] = math.sqrt(
                                float(columns[2]) ** 2
                                + float(columns[3]) ** 2
                                + float(columns[4]) ** 2
                            )
                            data["date"] = datetime.datetime.strptime(
                                f"{columns[0]} {columns[1]}", "%d/%m/%Y %H:%M:%S"
                            )
                            data["sensor"] = sensor.replace("_B_field", "")
                        except:
                            pass
                        result[i] = data
                        i += 1
    df = pd.DataFrame.from_dict(result, orient="index", dtype=float)
    df["date"] = pd.to_datetime(df["date"])
    df = df.set_index("date")
    df = df.groupby(["sensor"], group_keys=True).apply(lambda x: x)[
        ["Bx", "By", "Bz", "B"]
    ]
    return df


def plot_magnetic_fields(
    date_1: str,
    date_2: str,
    when="30S",
    save=False,
    file_name=None,
    show=True,
):
    df = get_magnetic_fields(date_1, date_2)
    keys = ["Bx", "By", "Bz", "B"]
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10, 10))
    for k in range(len(keys)):
        key = keys[k]
        n = bin(k).replace("b", "0")
        index = [int(n[-2]), int(n[-1])]
        data = (
            df[key]
            .reset_index()
            .pivot_table(index=["date"], columns=["sensor"], values=[key])
        )
        data[key].resample(when).mean().loc[date_1:date_2].plot(
            ax=axes[index[0], index[1]],
            kind="line",
            title=f"{key}",
            ylabel="Magnetic Field (µT)",
        )
        fig.tight_layout(pad=1.0)
    if save and file_name:
        plt.savefig(fname=file_name, dpi=300, format="png", bbox_inches="tight")
        plt.close()
    if show:
        plt.show()


# %%
days = 5  # days
today = datetime.date.today()
start_date = today - datetime.timedelta(days=days)
date_1, date_2 = start_date.strftime("%Y/%m/%d"), today.strftime("%Y/%m/%d")
plot_magnetic_fields(date_1, date_2)
